def main():
	from vault import read
	
	try:
		read('tests/test.psafe3', b'aaa')
	except ValueError:
		print('.')
	else:
		assert False, "should fail with invalid password"
	
	try:
		read('tests/test.psafe3', b'test123')
	except ValueError:
		assert False, "should work with correct password"
	else:
		print('.')

if __name__ == '__main__':
	main()
