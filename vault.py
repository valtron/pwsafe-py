from enum import IntEnum
from hashlib import sha256
from struct import unpack
from hmac import HMAC

from twofish.twofish_ecb import TwofishECB
from twofish.twofish_cbc import TwofishCBC

def read(filename: str, password: bytes) -> None:
	with open(filename, 'rb') as fh:
		tag = fh.read(4)
		if tag != b'PWS3':
			raise ValueError("Not a PasswordSafe V3 file")
		
		salt = fh.read(32)
		# ITER: SHA-256 keystretch iterations
		iter = unpack('<L', fh.read(4))[0]
		# P': the stretched key
		stretched_password = _stretch_password(password, salt, iter)
		my_sha_ps = sha256(stretched_password).digest()
		
		# H(P'): SHA-256 hash of stretched passphrase
		sha_ps = fh.read(32)
		if (sha_ps != my_sha_ps):
			raise ValueError("Wrong password")
		
		b1 = fh.read(16)
		b2 = fh.read(16)
		b3 = fh.read(16)
		b4 = fh.read(16)
		
		cipher = TwofishECB(stretched_password)
		key_k = cipher.decrypt(b1) + cipher.decrypt(b2)
		key_l = cipher.decrypt(b3) + cipher.decrypt(b4)
		
		iv = fh.read(16)
		hmac_checker = HMAC(key_l, b'', sha256)
		cipher = TwofishCBC(key_k, iv)
		
		is_header = True
		current_record = {}
		while True:
			(raw_type, raw_value) = _read_field(fh, cipher)
			if raw_type is None:
				break
			if raw_type == Field.END:
				if not is_header:
					group = current_record.get(Field.Group)
					title = current_record.get(Field.Title) or b''
					if group:
						title = group + b'.' + title
					title = title.decode('utf-8')
					user = current_record.get(Field.User).decode('utf-8')
					pwd = current_record.get(Field.Password).decode('utf-8')
				current_record = {}
				is_header = False
			else:
				hmac_checker.update(raw_value)
				if raw_type in Field.__members__.values():
					current_record[raw_type] = raw_value
		
		hmac = fh.read(32)
	
	if hmac != hmac_checker.digest():
		raise ValueError("File integrity check failed")

def _stretch_password(password: bytes, salt: bytes, iterations: int) -> bytes:
	"""
		Generate the SHA-256 value of a password after several rounds of stretching.
		
		The algorithm is described in the following paper:
		[KEYSTRETCH Section 4.1] http://www.schneier.com/paper-low-entropy.pdf
	"""
	stretched_password = password + salt
	for _ in range(iterations + 1):
		stretched_password = sha256(stretched_password).digest()
	return stretched_password

def _read_field(fh, cipher):
	"""
		Return one field of a vault record by reading from the given file handle.
	"""
	data = fh.read(16)
	if len(data) < 16:
		raise ValueError("EOF encountered when parsing record field")
	if data == b'PWS3-EOFPWS3-EOF':
		return (None, b'')
	data = cipher.decrypt(data)
	raw_len = unpack('<L', data[0:4])[0]
	raw_type = data[4]
	raw_value = data[5:]
	if raw_len > 11:
		for dummy in range((raw_len+4)//16):
			data = fh.read(16)
			if len(data) < 16:
				raise ValueError("EOF encountered when parsing record field")
			raw_value += cipher.decrypt(data)
	raw_value = raw_value[:raw_len]
	return (raw_type, raw_value)

class Field(IntEnum):
	Group = 0x02
	Title = 0x03
	User = 0x04
	Notes = 0x05
	Password = 0x06
	URL = 0x0D
	END = 0xFF
